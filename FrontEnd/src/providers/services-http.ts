import {Injectable} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";


let headers : any = new Headers();
headers.append('Content-Type', 'application/json');

@Injectable()
export class ServicesHttpProvider {

  constructor(private http: Http) {
  }

  genericPost(url: string, dataToSend?: object) {
    return this.http.post(url, dataToSend, headers );


  }

  genericGet(url: string, params?: object) {
    return this.http.get(url, {search: this.objToSearchParams(params)});
  }

  public mapResponse(res: Response) {
    //console.log(res);
    let body = res.json();
    return body || {};
  }

  private objToSearchParams(obj): URLSearchParams {
    let params: URLSearchParams = new URLSearchParams();
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        params.set(key, obj[key]);
    }
    return params;
  }

}
