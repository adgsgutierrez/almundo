export class ConstantProvider {

  private static ENDPOINT = 'http://localhost:3000';

  public static HOTELS_URI = ConstantProvider.ENDPOINT + '/hotels';
  public static IMAGE_URI = "assets/images/hotels/";

}
