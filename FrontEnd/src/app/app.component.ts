import { Component , AfterContentInit } from '@angular/core';
import { ServicesHttpProvider } from '../providers/services-http';
import { ConstantProvider } from '../providers/Constant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterContentInit {
  private load : boolean = true;
  private hotels = [];
  private estrellas ={
    uno : false,
    dos : false,
    tres: false,
    cuatro : false,
    cinco : false
  };

  constructor (private service : ServicesHttpProvider){
    this.hotels = [];
  }

  ngAfterContentInit(){
    console.log("co");
    this.buscarhotel();
  }

  public filtrarEstrellas():void{
      let starsArray =[];
      if(this.estrellas.uno){
        starsArray.push(1);
      }
      if(this.estrellas.dos){
        starsArray.push(2);
      }
      if(this.estrellas.tres){
        starsArray.push(3);
      }
      if(this.estrellas.cuatro){
        starsArray.push(4);
      }
      if(this.estrellas.cinco){
        starsArray.push(5);
      }
      this.load = true;
      this.hotels = [];
     this.service.genericGet(ConstantProvider.HOTELS_URI).subscribe((response)=>{
       let object : any[] = this.service.mapResponse(response);
       object.map((hotel)=>{
         if(starsArray.length == 0){
           let stars = [];
           for(let i = 0 ; i < hotel.stars ; i++){
             stars.push(i);
           }
           let tmp = {
              amenities : hotel.amenities ,
              id : hotel.id ,
              image : ConstantProvider.IMAGE_URI + hotel.image ,
              name : hotel.name ,
              price : hotel.price ,
              stars : stars ,
           }
           this.hotels.push(tmp);
         }
         if(starsArray.includes(hotel.stars)){
           let stars = [];
           for(let i = 0 ; i < hotel.stars ; i++){
             stars.push(i);
           }
           let tmp = {
              amenities : hotel.amenities ,
              id : hotel.id ,
              image : ConstantProvider.IMAGE_URI + hotel.image ,
              name : hotel.name ,
              price : hotel.price ,
              stars : stars ,
           }
           this.hotels.push(tmp);
         }
       });
       this.load = false;
     });
  }

  private buscarhotel():void{
    this.load = true;
    this.hotels = [];
     this.service.genericGet(ConstantProvider.HOTELS_URI).subscribe((response)=>{
       let object : any[] = this.service.mapResponse(response);
       object.map((hotel)=>{
         let stars = [];
         for(let i = 0 ; i < hotel.stars ; i++){
           stars.push(i);
         }
         let tmp = {
            amenities : hotel.amenities ,
            id : hotel.id ,
            image : ConstantProvider.IMAGE_URI + hotel.image ,
            name : hotel.name ,
            price : hotel.price ,
            stars : stars ,
         }
         this.hotels.push(tmp);
       });
       this.load = false;
     });
   }

  public filtrar(ev : any):void{
    this.load = true;
    this.hotels = [];
     this.service.genericGet(ConstantProvider.HOTELS_URI).subscribe((response)=>{
       let object : any[] = this.service.mapResponse(response);
       object.map((hotel)=>{
         let stars = [];
         if(ev.trim() == ""){
           for(let i = 0 ; i < hotel.stars ; i++){
             stars.push(i);
           }
           let tmp = {
              amenities : hotel.amenities ,
              id : hotel.id ,
              image : ConstantProvider.IMAGE_URI + hotel.image ,
              name : hotel.name ,
              price : hotel.price ,
              stars : stars ,
           }
           this.hotels.push(tmp);
         }else{
           if(hotel.name.indexOf(ev.trim()) > -1){
             for(let i = 0 ; i < hotel.stars ; i++){
               stars.push(i);
             }
             let tmp = {
                amenities : hotel.amenities ,
                id : hotel.id ,
                image : ConstantProvider.IMAGE_URI + hotel.image ,
                name : hotel.name ,
                price : hotel.price ,
                stars : stars ,
             }
             this.hotels.push(tmp);
           }
         }
         this.load = false;
       });
    });
  }
}
