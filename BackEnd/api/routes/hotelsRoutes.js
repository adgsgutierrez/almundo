'use strict';
module.exports = function(app) {
  var hotels = require('../controller/hotelsController');


  app.route('/hotels')
    .get(hotels.list_all)
    .post(hotels.create);


  app.route('/hotels/:hotelId')
    .put(hotels.update)
    .delete(hotels.delete);

};
