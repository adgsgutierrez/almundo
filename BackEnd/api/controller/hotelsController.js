'use strict';

  var hotels = require('../models/hotelsModel');

  exports.list_all = function(req, res) {
    res.json(hotels);
  };

  exports.create= function(req, res) {
    var hotel =   {
        "id": req.params.id,
        "name": req.params.name,
        "stars": req.params.starts,
        "price": req.params.price,
        "image": req.params.image,
        "amenities": req.params.amenities
      };
    hotels.put(hotel);
  };

  exports.update = function(req, res) {
    hotels.map( (hotel)=>{
      if(hotel.id == req.params.id){
        hotel =  {
           "id": req.params.id,
           "name": req.params.name,
           "stars": req.params.starts,
           "price": req.params.price,
           "image": req.params.image,
           "amenities": req.params.amenities
        };
      }
      hotels_tmp.push(hotel);
    });
    hotels = [];
    hotels = hotels_tmp;
  };

  exports.delete = function(req, res) {
    hotels_tmp = [];
    hotels.map( (hotel)=>{
      if(hotel.id != req.params.id){
        hotels_tmp.push(hotel);
      }
    });
    hotels = [];
    hotels = hotels_tmp;
  };
